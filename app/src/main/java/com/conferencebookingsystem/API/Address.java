package com.conferencebookingsystem.API;

public class Address {
    String street, zipCode, city;

    public Address(String street, String zipCode, String city) {
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
    }
}
